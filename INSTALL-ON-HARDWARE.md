# WORK IN PROGRESS -- THIS DOES NOT WORK AS INTENDED AT THIS POINT.


# Installing a Development Server On Hardware

This document guides you through the installation of a development server very similar to virtual private development server, but directly on hardware.


## Download Alpine Linux

Start by downloading Alpine Linux Extended version x86_64 from [https://alpinelinux.org/downloads/](https://alpinelinux.org/downloads/).


## Flash The Installation Image

After downloading the ISO file, you must flash the image onto a USB stick or hard disk.
**Be careful: all information on the USB stick or hard disk will be deleted.**

The easiest way to flash the image is to use **balenaEtcher** which can be downloaded for all major platforms from [https://www.nalena.io/etcher/](https://www.balena.io/etcher/).

Choose *Flash from file* and select the Alpine Linux ISO file you just downloaded.

Press *Select target* and select your USB stick.

> On macOS you might get be prompted for username and password to authenticate the operation, and then you get an error message. Simply choose the target again.

Press *Flash* and wait for the operation to finish.

Eject and remove the device from your computer.


## Start Installation

We will now begin installation of the development server on a real computer.
**Make sure any files on the computer you are using for this purpose is backed up and available to you on another computer.**

> We will not be concerned with having multiple operating systems on the computer at the same time. While this is certainly possible, the procedure differs between different hardware and already installed operating systems, and is out of scope for this installation guide.

Make sure the computer is turned off, and insert the USB stick into a USB port.

> As soon as the computer starts enter the computer settings - usually you need to press *F1* or something similar. You can find this information in your hardware documentation.

In the computer's settings, make sure hardware virtualization is turned on if you can change such a setting.

Next, make sure the computer allows booting from USB.

Save the settings if anything has changed, and exit.

Now, press any key according to your hardware documentation to select boot device (this is often *F12*).

Select the USB stick as a boot device, and then press *ENTER* on the Grub menu that follows.

You will now see a lot of text scrolling over the screen - this is normal as the computer is loading Alpine Linux.


## First Login

Your computer is now running Alpine Linux, but nothing has been installed yet.

Log in as *root* - you will not be prompted for a password so far.


## Delete File Systems

**Following this section will delete all files from your computer!**

First we must find the device name of your hard disk:

```bash
# fdisk -l | grep Disk
```

You will get information on your drives in your computer, but also the USB stick.
The drives will have names like `/dev/sda/` and `/dev/sdb/`.

The biggest drive should be your hard drive, and there should also be a drive with nearly the same capacity as your USB stick.

In most cases your hard drive is `/dev/sda/` and the USB stick will be `/dev/sdb/`.

To delete the file systems on `/dev/sda/` run this command **which will completely delete everything on the drive:**

```bash
# dd if=/dev/zero of=/dev/sda/ bs=512 count=1
```

You should see output like: `1+0 records in, 1+0 records out`.


## Run The Installer

Now run the installer:

```bash
# setup-alpine
```

Pay attention during the installation: make sure you select a proper keyboard layout, configure `wlan0` for WiFi, use `dhcp` for configuration of network, set time zone to `Europe` and `Copenhagen` - most other settings can be left as default.

Choose the disk for installation you just deleted above, e.g. `sda`, and set it up as `sys`.

After the installation run this command to reboot the system:

```bash
# reboot
```

Make sure to remove the USB stick when the screen blanks.


## Install VIM

We will need an editor to edit certain files during this guide.
We will use `vim` but you are free to install some other editor if you like.

Run this command:

```bash
# apk add vim
```


## Select Repositories

Run this command to edit the repositories list:

```bash
# vim /etc/apk/repositories
```

Remove the `#` sign in front of all lines beginning with `http://` by pressing `ESC` followed by `:%s/#//g` and press `ENTER`.
If the first line reads `/media/sdb/apks` press `ggdd` to delete that line.
Now press `:wq` to save and exit.


## Update Repositories

Run this command to update repositories:

```bash
# apk update
```

## Install A Bit More Software

We need a few more packages installed.

Run this command:

```bash
# apk add sudo git
```


## Add User

Working as the `root` user is not considered safe.
We must create a user for normal use:

```bash
# adduser henrik
```

Replace `henrik` with your user name.

We must allow the user to have `sudo` permissions, so he can act as `root`.

Type `visudo` at the terminal, and press `:/root` followed by `ENTER`.
Press `n` a few times until you see the line `root ALL=(ALL) ALL`.
Now press `o` and enter `henrik ALL=(ALL) NOPASSWD:ALL` (again, replace `henrik` with your user name) and press `ENTER` followed by `ESC`.

Save and exit by pressing `:wq`.


Type `exit` at the terminal to logout and log in as your new user.


## Clone Installation Repository

To clone the installation repository run this command:

```bash
$ git clone https://gitlab.com/henrikstroem/vps-reference-config.git
```


## Install Software

Run the following command to install some of the software we need:

```bash
$ sudo xargs apk add < vps-reference-config/install/apk-packages.txt
```

After running the command run `sudo reboot` to reboot the computer.


